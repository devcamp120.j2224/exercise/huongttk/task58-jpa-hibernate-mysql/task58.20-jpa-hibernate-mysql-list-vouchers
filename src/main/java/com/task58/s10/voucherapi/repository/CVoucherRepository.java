package com.task58.s10.voucherapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task58.s10.voucherapi.models.CVoucher;

public interface CVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
