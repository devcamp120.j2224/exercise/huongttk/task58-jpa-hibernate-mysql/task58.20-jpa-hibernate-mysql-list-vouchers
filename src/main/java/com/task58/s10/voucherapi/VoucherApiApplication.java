package com.task58.s10.voucherapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VoucherApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoucherApiApplication.class, args);
	}

}
