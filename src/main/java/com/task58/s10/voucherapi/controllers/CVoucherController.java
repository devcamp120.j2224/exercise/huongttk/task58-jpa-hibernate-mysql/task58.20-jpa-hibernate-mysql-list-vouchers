package com.task58.s10.voucherapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task58.s10.voucherapi.models.CVoucher;
import com.task58.s10.voucherapi.repository.CVoucherRepository;

@RestController
@CrossOrigin
@RequestMapping
public class CVoucherController {
    @Autowired
    CVoucherRepository voucherRepository;
    @GetMapping("vouchers")
    public ResponseEntity <List<CVoucher>> getAllVoucher(){
        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
            voucherRepository.findAll().forEach(listVoucher :: add);
            return new ResponseEntity<>(listVoucher, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }      
    }
}
